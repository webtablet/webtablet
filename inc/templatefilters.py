# http://daily.profeth.de/2008/04/using-custom-django-template-helpers.html

# import the webapp module
from google.appengine.ext import webapp
# get registry, we need it to register our filter later.
register = webapp.template.create_template_register()

# After we got the registry, we now can define our custom function:
def truncate(value,maxsize,stopper = '...'):
	"""	truncates a string to a given maximum
		size and appends the stopper if needed
	"""
	stoplen = len(stopper)
	if len(value) > maxsize and maxsize > stoplen:
		return value[:(maxsize-stoplen)] + stopper
	else:
		return value[:maxsize]

# Then, we need to register our filter as following:
register.filter(truncate)

# In the bootstrap file (the one with your main function):
# webapp.template.register_template_library('inc.templatefilters')

# In the template:
# {{ somevar|truncate:20 }}


def rb_quote_plus(value):
	"""	Replace special characters in string using
		the %xx escape. Letters, digits, and the
		characters '_.-' are never quoted. Also
		replaces spaces by plus signs, as required
		for quoting HTML form values.
	"""
	return urllib.quote_plus(value)

register.filter(rb_quote_plus)


def get_server_information(all,info = 1):
	if info == 1:
		return wsgiref.handlers.BaseHandler().os_environ
	else:
		return wsgiref.handlers.BaseHandler().os_environ.get(info)

register.filter(get_server_information)
# In template: {{ 1|get_server_information }}


def floatcomma(value):
	"""	Converts a float to a string containing commas every three digits.
		For example, 3000.65 becomes '3,000.65' and -45000.00 becomes '-45,000.00'.
	"""
	orig = force_unicode(value)
	intpart, dec = orig.split(".")
	intpart = intcomma(intpart)
	return ".".join([intpart, dec]) 

register.filter(floatcomma)


