#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
# import sys
import logging

# os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
#from google.appengine.dist import use_library
# use_library('django', '1.2')
#use_library('django', '0.96')


if os.environ['SERVER_SOFTWARE'].startswith('Dev'):	# dev environment
    _DEBUG = True	# trace libraries
    # sys.dont_write_bytecode = True	# new in Python 2.6
else:   # production
    _DEBUG = False
    from google.appengine.ext import ereporter	# daily exceptions cron mail
    ereporter.register_logger()


def logClient(title=None,descr=None,host=None):
	if not host:	host = os.environ['HTTP_HOST']	# failsave
	logging.info('%s%s%s%s%s%s%s ref=%s; addr=%s; agent=%s; charset=%s; accept=%s;' % (
		title+': ' if title else '', descr+': ' if descr else '',
		os.environ['REQUEST_METHOD']+' ' if os.environ['REQUEST_METHOD']!='GET' else '',
		'http://' if os.environ.get('HTTPS','off')=='off' else 'https://', host, os.environ['PATH_INFO'],
		'?'+os.environ['QUERY_STRING'] if ('QUERY_STRING' in os.environ  and os.environ['QUERY_STRING']) else '',
		os.environ.get('HTTP_REFERER','None')[:80],
		os.environ['REMOTE_ADDR'],
		os.environ.get('HTTP_USER_AGENT','None')[:60],
		os.environ.get('HTTP_ACCEPT_CHARSET','None')[:20],
		os.environ.get('HTTP_ACCEPT','None')[:40],
	))
	# cgi.print_environ_usage(),

def redir(url,status=303,title=None):
    if not title:	title = str(status)+' Redirect'	# +os.environ['HTTP_HOST']	# failsafe
    print 'Status: '+str(status)+'\nCache-Control: private,no-cache,max-age=0,must-revalidate\nPragma: no-cache\nContent-Type: text/html\nLocation: '+url
    print '\n<html><head><title>'+title+'</title><meta name="Robots" content="index,follow,noarchive"/></head><body>'
    print 'Redirect to: <a href="'+url+'">'+url+'</a>\n</body></html>'
    logging.debug('Redirect: status=%s; title=%s; url=%s; env=%s;' % (status, title, url, os.environ))
    logClient(title=title, descr=url)
    return

def main():

    if (not os.environ['REQUEST_METHOD']=='GET'):
        print 'Status 200\nCache-Control: private,no-cache,max-age=0,must-revalidate\n\n'
        return

    host = None
    http_host = os.environ['HTTP_HOST'].lower()
    path_info = os.environ['PATH_INFO']
    query_string = '?'+os.environ['QUERY_STRING'] if ('QUERY_STRING' in os.environ and os.environ['QUERY_STRING']) else ''

    if path_info.startswith('/rss/'):  path_info = path_info[4:]
    # elif path_info.startswith('/fsum/'):  path_info = path_info[5:]
    # elif path_info.startswith('/feedsum/'):  path_info = path_info[8:]
    else:
        path_info = '/'
        query_string = ''
        # exitMsg(status=203, title='Host '+http_host+' temporary offline', descr='under construction - please check back soon', host=http_host)	# Non-Authoritative

    # logging.debug('Message: status=%s; host=%s; msg=%s; env=%s;' % (status, host, msg, os.environ))
    #redir('http://'+http_host+path_info+query_string, status=303, title='FeedSum Redirect')
    #redir('http://'+http_host+path_info, status=301, title='FeedSum Redirect')
    # redir('http://feeds.mp3pod.tk/mp3pod_m3u?format=xml', status=302, title='')
    redir('http://feeds.feedburner.com%s?format=xml' % (path_info), status=302, title='')
    #redir('http://feeds.tablet.tk%s?format=xml' % (path_info), status=302, title='')
    return

if __name__=='__main__':
    main()
