
@rem set APP_HOME=%1%

@set APP_HOME=%~dp0
@rem set GAEPY=\PortableApps\GAEPyPortable
@set GAEPY=\_Authoring\AppEngine_Python
@set PYTHONHOME=%GAEPY%\App\python-2.5.4-gae

@rem set PYTHONOPTIMIZE=1
@set PYTHONNOUSERSITE=1

@cd %GAEPY%\App\google_appengine
%PYTHONHOME%\python.exe appcfg.py update %APP_HOME%
@cd %APP_HOME%

pause
